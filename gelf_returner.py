'''
Return gelf logs to a graylog server for logging

:maintainer:     Osprey Informatics <ospreyinformatics.com>
:maturity:       New
:depends:        gelfclient <https://github.com/orionvm/python-gelfclient>
:platform:       all

To enable this returner place the file in a directory called _returners within
your file_roots

The required configuration is as follows:

    gelf:
      gelf_host: 'somehost.example.com'
      gelf_port: 'your UDP gelf port number'
      gelf_mtu: maximum transmission unit
      gelf_sh_msg: 'a short message' (optional)
      gelf_f_msf: 'a full message' (optional)
      gelf_extras: (optional)
        - field1 : value1
        - field2: value2

The above configuration must be placed inside the minion configuration file;
gelf_extras allows you to add as many extra fields as you require within that
grain

To use the returner per salt call:

    salt '*' test.ping --return gelf
'''

import salt.config
import salt.loader

__opts__ = salt.config.minion_config('/etc/salt/minion')
__salt__ = salt.loader.minion_mods(__opts__)
__virtualname__ = 'gelf'

try:
    from gelfclient import UdpClient
    HAS_GCLIENT = True
except ImportError:
    HAS_GCLIENT = False

def __virtual__():
    '''
    Method called when module gets loaded
    '''
    if not HAS_GCLIENT:
        return False
    return __virtualname__


def addExtras(data_dict):
    '''
    Adds n numeber of user defined additional field under the gelf_extras grain
    '''
    for item in __salt__['config.get']('gelf:gelf_extras'):
        if isinstance(item, dict):
            for key, value in item.iteritems():
                data_dict.update({"_" + key : value})


def overall(data_dict):
    '''
    Calculates the duration of the entire state processing by all of the
    minions, the overall balance of succeded and failed states and the overall
    level of success of the state
    '''
    elapsed_time = 0.0
    number_ran = 0
    succeded = 0
    for inner in data_dict['return'].itervalues():
        elapsed_time += float(inner['duration'])
        if inner['result'] == True:
            succeded += 1
            number_ran += 1
        else:
            number_ran += 1
    data_dict.update({"_total_duration" : elapsed_time})
    data_dict.update({"_states_number" : number_ran})
    data_dict.update({"_states_succeded" : succeded})
    data_dict.update({"_states_failed" : number_ran - succeded})


def returner(data):
    '''
    Method called once module is loaded sends the data dictionary to
    the specified server
    '''
    if 'gelf_extras' in __salt__['config.get']('gelf'):
        if __salt__['config.get']('gelf:gelf_extras'):
            addExtras(data)
    if 'gelf_f_msg' in __salt__['config.get']('gelf'):
        data.update({"full_message" : __salt__['config.get']
                    ('gelf:gelf_f_msg')})
    if 'gelf_sh_msg' in __salt__['config.get']('gelf'):
        data.update({"short_message" : __salt__['config.get']
                    ('gelf:gelf_sh_msg')})
    else:
        data.update({"short_message" : data['id']})
    if type(data['return']) is dict:
        overall(data)
    gelf = UdpClient(
        __salt__['config.get']('gelf:gelf_host'),
        port = __salt__['config.get']('gelf:gelf_port'),
        mtu = __salt__['config.get']('gelf:gelf_mtu'),
        source = data['id']
    )
    gelf.log(data)
