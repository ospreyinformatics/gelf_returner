A [SaltStack](http://saltstack.com/) returner for returning data to [Graylog](https://www.graylog.org/) in Graylog Extended Log Format ([gelf](https://www.graylog.org/resources/gelf-2/)) format.

Returns of state modules (state.sls or state.highstate) will also summarize the total duration of the run as well as the number of states executed, succeeded and failed.  

### Usage ###

Place gelf_returner.py inside the _returners directory in your file_roots (see [here](http://docs.saltstack.com/en/latest/ref/returners/#using-returners) for more information).

For example assuming base is defined in `/etc/salt/master` below:
```yaml  
file_roots:
  base:
    - /srv/salt
```

`_returners` should be included as a sub-directory of `/srv/salt`
```bash
srv
└── salt
    ├── _returners
    └── top.sls
```

### Configuration

The gelf_returner expects configuration information to be read from the minion configuration. If you are deploying to existing minions it may be easier to add the configuration to a file and place it in the `/etc/salt/minion.d` directory.

`/etc/salt/minion.d/gelf.conf`
```yaml
gelf:
    gelf_host: 'somehost.example.com'
    gelf_port: 'your UDP gelf port number'
    gelf_mtu: maximum transmission unit
    gelf_sh_msg: 'a short message' (optional)
    gelf_f_msf: 'a full message' (optional)
    gelf_extras: (optional)
      - field1 : value1
      - field2: value2
```
